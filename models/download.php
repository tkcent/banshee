<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class download_model extends Banshee\model {
		public function get_mimetype($file) {
			if (file_exists("/etc/mime.types") == false) {
				return false;
			}

			$info = pathinfo($file);
			$file_ext = $info["extension"];

			foreach (file("/etc/mime.types") as $line) {
				$line = trim($line);
				if (($line == "") || (substr($line, 0, 1) == "#")) {
					continue;
				}

				$line = preg_replace('/\s+/', ' ', $line);
				$extensions = explode(" ", $line);
				$mimetype = array_shift($extensions);

				if (in_array($file_ext, $extensions)) {
					return $mimetype;
				}
			}

			return "application/x-binary";
		}
	}
?>
