<?php
	class dynamic_page_blocks extends Banshee\dynamic_blocks {
		protected $xslt_path = "views/demos";

		protected function poll_sidebar() {
			$poll = new \Banshee\poll($this->db, $this->view, $this->settings, $this->user);
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$poll->vote($_POST["vote"]);
			}
			$poll->add_to_view();
		}

		protected function timestamp() {
			$this->view->add_tag("time", date_string("j F Y, H:i:s"));
		}
	}
?>
